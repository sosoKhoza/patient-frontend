package com.patient.frontend.client;

import java.util.List;

import org.springframework.stereotype.Component;

import com.patient.frontend.model.Patient;

@Component
public class JSONPlaceHolderFallback implements JSONPlaceHolderClient {

	@Override
	public List<Patient> getPatients() {
		return null;
	}

	@Override
	public List<Patient> getPerson(String searchTerm, String searchType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void saveProduct(Patient patient) {
	
	}
 
}
