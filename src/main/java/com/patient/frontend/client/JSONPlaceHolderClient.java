package com.patient.frontend.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.patient.frontend.model.Patient;


@FeignClient(value = "patient-api", url = "http://localhost:8181/patient/", fallback = JSONPlaceHolderFallback.class)
public interface JSONPlaceHolderClient {
	@GetMapping("/patients")
	public List<Patient> getPatients();
	@GetMapping("/search")
	public List<Patient> getPerson(@RequestParam String searchTerm, @RequestParam String searchType);
	@RequestMapping(value = "/add", method = RequestMethod.POST, produces = "application/json")
	public void saveProduct(@RequestBody Patient patient);

}
