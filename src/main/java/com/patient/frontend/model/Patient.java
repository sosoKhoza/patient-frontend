package com.patient.frontend.model;

import java.time.LocalDate;

public class Patient {
private String patientId;
	
	private String name;
	private String surname;
	private LocalDate age;
	
	public String getPatientId() {
		return patientId;
	}
	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public LocalDate getAge() {
		return age;
	}
	public void setAge(LocalDate age) {
		this.age = age;
	}
	
	@Override
	public String toString() {
		return "Patient [patientId=" + patientId + ", name=" + name + ", surname=" + surname + ", age=" + age + "]";
	}

}
