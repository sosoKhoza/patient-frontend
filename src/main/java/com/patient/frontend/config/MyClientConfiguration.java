package com.patient.frontend.config;

import org.apache.http.entity.ContentType;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.patient.frontend.exception.CustomErrorDecoder;

import feign.Logger;
import feign.RequestInterceptor;
import feign.codec.ErrorDecoder;
import feign.okhttp.OkHttpClient;

@FeignClient(value = "patientApi",
url = "http://localhost:8181/patient/",
configuration = MyClientConfiguration.class)
@Configuration
public class MyClientConfiguration {
	
	 @Bean
	    public OkHttpClient client() {
	        return new OkHttpClient();
	    }
	 
	 @Bean
	 public RequestInterceptor requestInterceptor() {
	   return requestTemplate -> {
	       requestTemplate.header("user", "user");
	       requestTemplate.header("password", "password");
	       requestTemplate.header("Accept", ContentType.APPLICATION_JSON.getMimeType());
	   };
	 }
	 
	 @Bean
	    Logger.Level feignLoggerLevel() {
	        return Logger.Level.BASIC;
	    }
	 
	 @Bean
	    public ErrorDecoder errorDecoder() {
	        return new CustomErrorDecoder();
	    }

}
